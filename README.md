<img src="pictures/Delephant_Schrift.png" alt="delephant" width="30%" height="30%">

Delephant helps you clean out your Elasticsearch-mess.
This tool allows you to sort out old data. Just set it to whatever you like (for example older than a week: 7D).

It also comes with a built in schedule which allows it to run every day. Or you can just skip the configuration and only run it once.

The logo was designed by [Drahgons](https://www.deviantart.com/drahgons/)